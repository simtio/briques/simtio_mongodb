# SIMTIO - Mongodb

## Dev
```
docker build -t registry.gitlab.com/simtio/briques/simtio_mongodb:latest -t registry.gitlab.com/simtio/briques/simtio_mongodb:0.x .

docker run --name simtio_mongodb -d registry.gitlab.com/simtio/briques/simtio_mongodb:latest

docker push registry.gitlab.com/simtio/briques/simtio_mongodb && docker push registry.gitlab.com/simtio/briques/simtio_mongodb:0.x
```

## Sources
- https://github.com/docker-library/mongo
- https://hub.docker.com/_/mongo/
