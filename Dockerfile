#################################
## Dockerfile Mongodb          ##
## SIMTIO                      ##
#################################

# Get SIMTIO template image
FROM registry.gitlab.com/simtio/simtio_base_image:latest

# grab gosu for easy step-down from root (https://github.com/tianon/gosu/releases)
ENV GOSU_VERSION 1.11
# grab "js-yaml" for parsing mongod's YAML config files (https://github.com/nodeca/js-yaml/releases)
ENV JSYAML_VERSION 3.13.0
ENV GPG_KEYS 9DA31620334BD75D9DCB49F368818C72E52529D4

RUN apt-get update; \
    apt-get install --no-install-recommends --no-install-suggests -qy \
		jq numactl procps dirmngr wget; \
    groupadd -r mongodb && useradd -r -g mongodb mongodb; \
    dpkgArch="$(dpkg --print-architecture | awk -F- '{ print $NF }')"; \
    wget -O /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch"; \
    wget -O /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$dpkgArch.asc"; \
    export GNUPGHOME="$(mktemp -d)"; \
    gpg --batch --keyserver hkps://keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4; \
    gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu; \
    command -v gpgconf gpgconf --kill all || :; \
    rm -r "$GNUPGHOME" /usr/local/bin/gosu.asc; \
    chmod +x /usr/local/bin/gosu; \
    gosu --version; \
    gosu nobody true; \
    wget -O /js-yaml.js "https://github.com/nodeca/js-yaml/raw/${JSYAML_VERSION}/dist/js-yaml.js"; \
    mkdir /docker-entrypoint-initdb.d; \
    export GNUPGHOME="$(mktemp -d)"; \
  	for key in $GPG_KEYS; do \
  		gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "$key"; \
  	done; \
    gpg --batch --export $GPG_KEYS > /etc/apt/trusted.gpg.d/mongodb.gpg; \
    command -v gpgconf gpgconf --kill all || :; \
    rm -r "$GNUPGHOME";

# Allow build-time overrides (eg. to build image with MongoDB Enterprise version)
# Options for MONGO_PACKAGE: mongodb-org OR mongodb-enterprise
# Options for MONGO_REPO: repo.mongodb.org OR repo.mongodb.com
# Example: docker build --build-arg MONGO_PACKAGE=mongodb-enterprise --build-arg MONGO_REPO=repo.mongodb.com .
ARG MONGO_PACKAGE=mongodb-org
ARG MONGO_REPO=repo.mongodb.org
ENV MONGO_PACKAGE=${MONGO_PACKAGE} MONGO_REPO=${MONGO_REPO}

ENV MONGO_MAJOR 4.0
ENV MONGO_VERSION 4.0.13

COPY entrypoint.sh /

RUN echo "deb http://$MONGO_REPO/apt/debian stretch/${MONGO_PACKAGE%}/$MONGO_MAJOR main" | tee "/etc/apt/sources.list.d/${MONGO_PACKAGE%}.list" \
    && apt-get update && apt-get install -qy \
		${MONGO_PACKAGE}=$MONGO_VERSION \
		${MONGO_PACKAGE}-server=$MONGO_VERSION \
		${MONGO_PACKAGE}-shell=$MONGO_VERSION \
		${MONGO_PACKAGE}-mongos=$MONGO_VERSION \
		${MONGO_PACKAGE}-tools=$MONGO_VERSION \
	  && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/lib/mongodb \
    && apt-get clean \
    && mv /etc/mongod.conf /etc/mongod.conf.orig \
    && mkdir -pv /data/db /data/configdb \
    && chown -R mongodb:mongodb /data/db /data/configdb \
    && chmod +x /entrypoint.sh;

VOLUME /data/db /data/configdb

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 27017
CMD ["mongod"]
